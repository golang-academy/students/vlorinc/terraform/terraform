#!/bin/bash

apt update

apt install -y nginx

echo "Welcome to Terraform created web server" > /var/www/html/index.html

# Create User
useradd -s /bin/bash -c "tux" -m tux
echo "Passw0rd" | passwd --stdin tux
# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/tux/.ssh
echo "ssh-ed25519 xxxx" >> /home/tux/.ssh/authorized_keys
# Set proper permissions
chown -R tux:tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

# install k3s
#curl -sfL https://get.k3s.io | sh -
