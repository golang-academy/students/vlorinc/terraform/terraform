# Control node
resource "aws_instance" "control" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "10.0.0.4"
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "control"
  }
}

# Worker node 1
resource "aws_instance" "worker1" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "10.0.0.1"
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker1"
  }
}

# Worker node 2
resource "aws_instance" "worker2" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "10.0.0.2"
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker2"
  }
}

# Worker node 3
resource "aws_instance" "worker3" {
  ami             = var.image
  instance_type   = var.instance
  //subnet_id       = aws_subnet.dmz.id
  //private_ip      = "10.0.0.3"
  user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "worker3"
  }
}
