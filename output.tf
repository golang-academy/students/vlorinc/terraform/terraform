output "instance_public_ip" {
  description = "Public IP of EC2 instance"
  value       = aws_instance.control.public_ip
}
/*
output "instance_public_ip_worker" {
  description = "Public IP of EC2 instance"
  value       = aws_instance.worker*.public_ip
}
*/
